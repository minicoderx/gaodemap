package com.amap.map3d.demo.overlay;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.map3d.demo.R;
import com.autonavi.base.amap.mapcore.Convert;

/**
 * AMapV2地图中简单介绍一些Marker的用法.
 */
public class MarkerActivity extends Activity implements OnClickListener {
    private MarkerOptions markerOption;
    private AMap aMap;
    private MapView mapView;
    private LatLng latlng = new LatLng(39.761, 116.434);
    Marker marker;

    private boolean mIsMarkerSelect = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marker_activity);
        /*
         * 设置离线地图存储目录，在下载离线地图或初始化地图设置; 使用过程中可自行设置, 若自行设置了离线地图存储的路径，
         * 则需要在离线地图下载和使用地图页面都进行路径设置
         */
        // Demo中为了其他界面可以使用下载的离线地图，使用默认位置存储，屏蔽了自定义设置
        // MapsInitializer.sdcardDir =OffLineMapUtils.getSdCacheDir(this);
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState); // 此方法必须重写
        init();
    }

    /**
     * 初始化AMap对象
     */
    private void init() {
        Button clearMap = (Button) findViewById(R.id.clearMap);
        clearMap.setOnClickListener(this);
        Button resetMap = (Button) findViewById(R.id.resetMap);
        resetMap.setOnClickListener(this);
        if (aMap == null) {
            aMap = mapView.getMap();
            addMarkersToMap();// 往地图上添加marker
            final GestureDetector detector = new GestureDetector(this, new EventListener());
            aMap.addOnMapTouchListener(new AMap.OnMapTouchListener() {
                @Override
                public void onTouch(MotionEvent motionEvent) {
                    detector.onTouchEvent(motionEvent);
                }
            });
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /**
     * 在地图上添加marker
     */
    private void addMarkersToMap() {

        markerOption = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car))
                .position(latlng);
        marker = aMap.addMarker(markerOption);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        /**
         * 清空地图上所有已经标注的marker
         */
        case R.id.clearMap:
            if (aMap != null) {
                marker.destroy();
            }
            break;
        /**
         * 重新标注所有的marker
         */
        case R.id.resetMap:
            if (aMap != null) {
                aMap.clear();
                addMarkersToMap();
            }
            break;
        default:
            break;
        }
    }

    private class EventListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (mIsMarkerSelect) {
                Log.i("LJJ", "onScroll e1:" + e1.toString());
                Log.i("LJJ", "onScroll e2:" + e2.toString());
                Log.i("LJJ", "distanceX=" + distanceX);
                Log.i("LJJ", "distanceY=" + distanceY);
                Point point = new Point((int) e2.getX(), (int) e2.getY());
                LatLng latLng = aMap.getProjection().fromScreenLocation(point);
                marker.setPosition(latLng);
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.i("LJJ", "e1 Fling:" + e1.toString());
            Log.i("LJJ", "e2 Fling:" + e2.toString());
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Point point = new Point((int) e.getX(), (int) e.getY());
            int offset = 30;
            Point sw = new Point(point.x - offset, point.y + offset);
            Point ne = new Point(point.x + offset, point.y - offset);
            LatLng latLngSW = aMap.getProjection().fromScreenLocation(sw);
            LatLng latLngNE = aMap.getProjection().fromScreenLocation(ne);
            aMap.addMarker(new MarkerOptions().position(latLngSW));
            aMap.addMarker(new MarkerOptions().position(latLngNE));
            LatLngBounds bounds = new LatLngBounds(latLngSW, latLngNE);
            if (bounds.contains(marker.getPosition())) {
                Log.i("LJJ", "marker位置包含");
                mIsMarkerSelect = true;
                aMap.getUiSettings().setScrollGesturesEnabled(false);
            } else {
                mIsMarkerSelect = false;
                aMap.getUiSettings().setScrollGesturesEnabled(true);
            }
            return true;


        }
    }
}
